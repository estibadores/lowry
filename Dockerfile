FROM registry.sindominio.net/debian as builder

ARG BRANCH=master

RUN apt-get update && \
    apt-get install -y --no-install-recommends git golang npm make

RUN git clone --branch ${BRANCH} https://git.sindominio.net/sindominio/lowry

WORKDIR /lowry

RUN make js
run CGO_ENABLED=0 go build -ldflags '-extldflags "-static"' .

FROM scratch

ENV TZ=Europe/Madrid

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /lowry/lowry /lowry
COPY --from=builder /lowry/dist /dist
COPY --from=builder /lowry/tmpl /tmpl
COPY --from=builder /lowry/img /img
COPY --from=builder /lowry/examples/lowry.conf /etc/lowry.conf

ENTRYPOINT ["/lowry"]
